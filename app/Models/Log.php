<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Log
 *
 * @package App\Models
 *
 * @property Carbon starttime
 * @property int pageid
 * @property string eventname
 * @property string target
 * @property int targetid
 * @property int uid
 */
class Log extends Model
{
    public $primaryKey = '';

    public $incrementing = false;

    public $timestamps = false;

    public $fillable = [
        'starttime',
        'pageid',
        'eventname',
        'target',
        'targetid',
        'uid',
    ];

    public $dates = [
        'starttime',
    ];
}
