<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 05.03.2020
 * Time: 10:44
 */

namespace App\Services;

use App\Contracts\IUrlChecker;
use App\Models\WebPage;
use Illuminate\Support\Str;

class UrlChecker implements IUrlChecker
{
    public function getAllLinks(WebPage $page) : array
    {
        $domain = $this->getUrlAbsolute($page->getUrl(), null, true);
        $results = [];

        $links = $page->getDomDoc()->getElementsByTagName('a');
        /* @var $link \DOMElement */
        foreach ($links as $link) {
            $linkUrl = $link->getAttribute('href');

            $results[] = $this->getUrlAbsolute($linkUrl, $domain);
        }

        return $results;
    }

    public function getBrokenImages(WebPage $page) : array
    {
        $domain = $this->getUrlAbsolute($page->getUrl(), null, true);
        $results = [];

        $images = $page->getDomDoc()->getElementsByTagName('img');
        /* @var $image \DOMElement */
        foreach ($images as $image) {
            $imgUrl = $this->getUrlAbsolute($image->getAttribute('src'), $domain);
            $isImgAvailable = WebPage::isUrlOfCType($imgUrl, [
                'image' => '*',
                'application' => [
                    'svg',
                    'svg+xml',
                ]
            ]);
            if (!$isImgAvailable) {
                $results[] = $imgUrl;
            }
        }

        return $results;
    }

    public function getUrlAbsolute(string $url, ?string $hostDefault = null, bool $domainOnly = false) : string
    {
        $urlParts = parse_url($url);

        $scheme = isset($urlParts['scheme']) && !empty($urlParts['scheme']) ? $urlParts['scheme'] . '://' : ($hostDefault ? '': 'http://');
        $host = @$urlParts['host'] ?: ($hostDefault ?: (@$urlParts['path'] ? head(explode('/', @$urlParts['path'])): ''));
        $port = @$urlParts['port'] ?: '';
        $domainParts = "$scheme$host$port";

        if (!Str::startsWith($domainParts, 'http')) {
            $domainParts = 'http://' . $domainParts;
        }
        $domainParts = rtrim($domainParts, '/') . '/';

        if ($domainOnly) {
            return $domainParts;
        }

        $path = @$urlParts['path'] ?: '';
        $path = ltrim($path, '/');
        if (!@$urlParts['host']) {
            $path = '';
        }
        $query = isset($urlParts['query']) ? '?' . $urlParts['query'] : '';
        $additionalParts = "$path$query";

        return "$domainParts$additionalParts";
    }
}