<?php

use App\Enums\LogModelEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->dateTime('starttime')->useCurrent();
            $table->unsignedInteger('pageid');
            $table->enum('eventname', LogModelEnum::EVENTS);
            $table->enum('target', LogModelEnum::TARGETS);
            $table->unsignedInteger('targetid');
            $table->unsignedInteger('uid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
