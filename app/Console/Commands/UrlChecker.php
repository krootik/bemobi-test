<?php

namespace App\Console\Commands;

use App\Contracts\IUrlChecker;
use App\Models\WebPage;
use Illuminate\Console\Command;

class UrlChecker extends Command
{
    /* @var $urlCheckeк \App\Services\UrlChecker */
    protected $checkerInstace;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:checker {url : Url to check. Any format} {--get=links : Type of output. May be [links|images|both]}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(IUrlChecker $urlCheckeк)
    {
        parent::__construct();
        $this->checkerInstace = $urlCheckeк;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = $this->argument('url');
        $displayResult = strtolower($this->option('get'));
        try {
            $page = new WebPage($url);

            if (in_array($displayResult, ['links', 'both'])) {
                $links = $this->checkerInstace->getAllLinks($page);
                $this->info('Links with absolute path:');
                if (empty($links)) {
                    $this->info('There are no links');
                } else {
                    foreach ($links as $link) {
                        $this->line($link);
                    }
                }
            }
            $displayResult === 'both' && $this->line('');
            if (in_array($displayResult, ['images', 'both'])) {
                $imgs = $this->checkerInstace->getBrokenImages($page);
                $this->info('Broken images:');
                if (empty($imgs)) {
                    $this->info('There are no broken images');
                } else {
                    foreach ($imgs as $img) {
                        $this->line($img);
                    }
                }
            }
        } catch (\Throwable $e) {
            $this->error($e->getMessage());
        }
    }
}
