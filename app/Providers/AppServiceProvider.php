<?php

namespace App\Providers;

use App\Contracts\IUrlChecker;
use App\Services\UrlChecker;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IUrlChecker::class, static function($app) {
            return new UrlChecker();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
