<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Enums\LogModelEnum;
use App\Models\Log;
use Faker\Generator as Faker;
use Illuminate\Support\Arr;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Log::class, static function (Faker $faker) {
    $pageIds = range(1, 10);
    $targets = range(1, 30);
    $users = range(1, 20);
    return [
//        '' => null,
        'starttime' => $faker->dateTimeInInterval('-5 days'),
        'pageid' => Arr::random($pageIds),
        'eventname' => Arr::random(LogModelEnum::EVENTS),
        'target' => Arr::random(LogModelEnum::TARGETS),
        'targetid' => Arr::random($targets),
        'uid' => Arr::random($users),
    ];
});
