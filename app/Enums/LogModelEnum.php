<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 05.03.2020
 * Time: 13:54
 */

namespace App\Enums;

final class LogModelEnum
{
    public const EVENT_VIEW = 'view';
    public const EVENT_CLICK = 'click';
    public const EVENTS = [
        self::EVENT_CLICK,
        self::EVENT_VIEW,
    ];

    public const TARGET_PAGE = 'page';
    public const TARGET_BANNER = 'banner';
    public const TARGETS = [
        self::TARGET_PAGE,
        self::TARGET_BANNER,
    ];
}