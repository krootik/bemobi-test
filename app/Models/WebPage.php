<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 05.03.2020
 * Time: 10:43
 */

namespace App\Models;

use App\Contracts\IUrlChecker;
use Illuminate\Support\Str;

class WebPage
{
    protected $url;
    protected $document;

    public function __construct(string $url)
    {
        $url = resolve(IUrlChecker::class)->getUrlAbsolute($url);
        if (!self::isUrlStatusExists($url)) {
            throw new \Exception('Url not available at the moment');
        }
        $this->url = $url;

        $this->document = new \DOMDocument();
        @$this->document->loadHTML(
            file_get_contents(
                $this->url,
                false,
                stream_context_create(
                    [
                        'http' => ['follow_location' => true]
                    ]
                )
            )
        );
    }

    public function getUrl() : string
    {
        return $this->url;
    }

    public function getDomDoc() : \DOMDocument
    {
        return $this->document;
    }

    public static function getUrlHeaders(string $url) : array
    {
        $curlResource = curl_init($url);
        curl_setopt_array($curlResource, [
            CURLOPT_HEADER => true,
            CURLOPT_NOBODY => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => false
        ]);

        return preg_split('/[\r\n]+/', curl_exec($curlResource));
    }

    public static function isUrlStatusExists(string $url, ?array $urlHeaders = null) : bool
    {
        if (null === $urlHeaders) {
            $urlHeaders = self::getUrlHeaders($url);
        }
        $httpStatusHeader = array_shift($urlHeaders);
        $pregRes = [];
        $match = preg_match('/^http.*\b(?<code>\d+)\b[\s\w]*/i', $httpStatusHeader, $pregRes);

        return $match && in_array((int) @$pregRes['code'], [200, 301, 302]);
    }

    public static function isUrlOfCType(string $url, array $cTypeByGroup) : bool
    {
        $urlHeaders = self::getUrlHeaders($url);

        if (!self::isUrlStatusExists($url, $urlHeaders)) {
            return false;
        }

        foreach ($urlHeaders as $header) {
            $pregRes = [];
            if (preg_match('/^content-type: (?<group>\w+)\/(?<type>[\w\+]+);?.*/i', $header, $pregRes)) {
                foreach ($cTypeByGroup as $group => $type) {
                    if (Str::lower($group) === Str::lower($pregRes['group'])) {
                        $type = (array) $type;
                        if (in_array('*', $type) || Str::contains($pregRes['type'], $type)) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }
}