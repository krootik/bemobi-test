<?php
/**
 * Created by PhpStorm.
 * User: krootik
 * Date: 05.03.2020
 * Time: 10:40
 */

namespace App\Contracts;

use App\Models\WebPage;

interface IUrlChecker
{
    public function getBrokenImages(WebPage $page): array;
    public function getAllLinks(WebPage $page): array;
}