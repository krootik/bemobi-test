<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PageVisitors extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:visitors {--query : See raw query}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pureQuery = '
        SELECT
            DATE(starttime) AS by_date,
            pageid,
            SUM(eventname = \'view\') AS view_count,
            SUM(eventname = \'click\') AS click_count,
            (
                SUM(eventname = \'click\') / SUM(eventname = \'view\')
            ) * 100 AS ctr,
            COUNT(DISTINCT uid) AS unique_users,
            COUNT(
                DISTINCT IF (eventname = \'click\', uid, NULL)
            ) AS clickers
        FROM
            `logs`
        GROUP BY
            by_date,
            pageid
        ORDER BY
            by_date,
            pageid
            ';
        if ($this->option('query')) {
            $this->output->write($pureQuery);
        } else {
            $allStatistics = (array) DB::select($pureQuery);
            $headers = array_keys((array) head($allStatistics));
            $allStatistics = array_map(static function ($arr) {return (array) $arr;}, $allStatistics);
            $this->table($headers, $allStatistics);
        }
    }
}
